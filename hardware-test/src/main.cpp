#include <EnableInterrupt.h>
#include <Arduino.h>
#include <ServoTimer2.h>

#define TEST_BTN

/*
#define TEST_LED
#define TEST_POT
#define TEST_PIR
#define TEST_MOT
#define TEST_SON
*/

/* LEDS */
#ifdef TEST_LED
  #define LED_COUNT 2
  #define PIN_LA 5
  #define PIN_LD 6
  int leds[LED_COUNT] = {PIN_LA, PIN_LD};
  #define BLINK_DURATION 100
#endif

/* BUTTONS */
/* NOTE: interrupt buttons must be on appropriate LEDs */
#ifdef TEST_BTN
  #define BTN_COUNT 3
  #define PIN_B1 2
  #define PIN_B2 3
  #define PIN_B4 4
  int btns[BTN_COUNT] = {PIN_B1, PIN_B2, PIN_B4};
  #define MSG_BTN "[!] Button pressed!"
#endif

/* POTENTIOMETERS */
#ifdef TEST_POT
  #define POT_COUNT 1
  #define PIN_POT1 A0

  int pots[POT_COUNT] = {PIN_POT1};

  #define MSG_POT(X) "[!] POT VALUE = " + (String)X
#endif

/* PIR */
#ifdef TEST_PIR
  #define PIR_COUNT 1
  #define PIN_PIR1 7
  int pirs[PIR_COUNT] = {PIN_PIR1};
  #define MSG_PIR(X) "[!] PIR VALUE = " + (String)X
  #define PIR_CALIBRATION_TIME_SEC 10
#endif

/* MOTORS */
#ifdef TEST_MOT
  #define MOT_COUNT 
  #define PIN_MOT1 12
  int mot[MOT_COUNT] = {PIN_MOT1};
  #define MSG_MOT(X) "[!] MOVING MOTOR = " + (String)X
  int pos;
  int delta;
  ServoTimer2 motor;
#endif

/* ULTRASONIC */
#ifdef TEST_SON
  #define PIN_TRIG 10
  #define PIN_ECHO 9
  #define VS (331.5 + 0.6 * 20)
 #endif

/* UTILS */
#ifdef TEST_LED
void blinkLed(uint8_t led, unsigned long duration) { 
  digitalWrite(led, HIGH);
  delay(duration);
  digitalWrite(led, LOW);
  delay(duration);
}
#endif

/* TESTS */
#ifdef TEST_POT
void testPotentiometer() {
  for(int i = 0; i < POT_COUNT; i++) {
    Serial.println(MSG_POT(analogRead(pots[i])));
  }
}
#endif

#ifdef TEST_LED
void testLed() {
  for(int i = 0; i < LED_COUNT; i++) {
    blinkLed(leds[i], BLINK_DURATION);
  }
}
#endif

#ifdef TEST_BTN
void testButton() {
  Serial.println(MSG_BTN);
}
#endif

#ifdef TEST_PIR
void testPir() {
  for(int i = 0; i < PIR_COUNT; i++) {
    int detected = digitalRead(pirs[i]);
    if (detected == HIGH){
      Serial.println("detected!");
    } else
    {
      Serial.println("NOT detected!");
    }
    delay(500);
  }
}
#endif

#ifdef TEST_SON
float getDistance() {
    /* invio impulso */
    digitalWrite(PIN_TRIG, LOW);
    delayMicroseconds(3);
    digitalWrite(PIN_TRIG, HIGH);
    delayMicroseconds(5);
    digitalWrite(PIN_TRIG, LOW);

    /* ricevi l’eco */
    float tUS = pulseIn(PIN_ECHO, HIGH);
    float t = tUS / 1000.0 / 1000.0 / 2;
    float d = t * VS;
    return d;
}


void testUltrasonic() {
  float d = getDistance();
  Serial.println((String) "[!] ULTRA = " + d);
  delay(200);
}
#endif

#ifdef TEST_MOT
void testMotor() {
  motor.attach(PIN_MOT1);
  for (int i = 0; i < 180; i++) {
    Serial.println(pos);
    float coeff = (2250.0 - 750.0) / 180;
    motor.write(750 + pos*coeff);
    delay(15);
    pos += delta;
  }
  motor.detach();
  pos -= delta;
  delta = -delta;
  delay(1000);
}
#endif

/* END TESTS */

void setupTest() {
  Serial.println("setupTest");

  /* LED */
  #ifdef TEST_LED
  for(int i = 0; i < LED_COUNT; i++) {
     pinMode(leds[i], OUTPUT);
  }
  #endif

  /* BUTTON */
  #ifdef TEST_BTN
  for(int i = 0; i < BTN_COUNT; i++) {
    Serial.println("Attaching btn interrupt to pin" + btns[i]);
    // attachInterrupt(digitalPinToInterrupt(btns[i]), testButton, RISING);
    enableInterrupt(btns[i], testButton, RISING);
  }
  #endif

  /* PIR */
  #ifdef TEST_PIR
  for(int i = 0; i < PIR_COUNT; i++) {
    pinMode(pirs[i], INPUT);
  }

  Serial.print("Calibrating sensors...");
  for(int j = 0; j < PIR_CALIBRATION_TIME_SEC; j++){
    Serial.print(".");
    delay(1000);
  }
  Serial.println(" done");
  Serial.println("PIR SENSORS READY.");
  delay(50);
  #endif

  /* ULTRASONIC */
  #ifdef TEST_SON
  pinMode(PIN_TRIG, OUTPUT);
  pinMode(PIN_ECHO, INPUT);
  #endif

  /* MOTOR */
  #ifdef TEST_MOT
  pos = 0;
  delta = 1;
  #endif
}

void setup() {
  Serial.begin(9600);
  Serial.println("start setup()");
  setupTest();
  Serial.println("end setup()");
}

void loop() {
  // Serial.println("start loop()");
  #ifdef TEST_LED
  testLed();
  #endif

  #ifdef TEST_POT
  testPotentiometer();
  #endif

  #ifdef TEST_PIR
  testPir();
  #endif

  #ifdef TEST_SON
  testUltrasonic();
  #endif

  #ifdef TEST_MOT
  testMotor();
  #endif
}