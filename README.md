# IoT 2019/2020

[[_TOC_]]

## Exam tips

### Interview

Tips:
- you have to know everything, not in a really in-depth level, but everything
- do NOT say "i do not remember the exact number...", instead try to say the scale
  or a range

Typical questions (**NOTE**: lot of them are also explained in lab slides)
- what is the typical frequency of a microcontroller (e.g. Arduino)?
  - can ask only the scale, kHz, MHz, GHz
- what is polling?
- what is interrupt?
- what's the difference between pollign and interrupt?
- what are polling and interrupt used for?
  - reading data from sensors (each one in its way)
- what problems do polling and interrupt have?
  - polling: if an event is triggered multiple times, the polling read could not
    read all the changes (_event loss_). That problem can be solved using interrups.
  - interrupts are not compatible with atomic events, so if you have a critical section
    (e.g. atomic access to a variable) you would want to disable interrupts, make 
    the change and re-enable interrupts (**NOTE**: disabling interrupts also stops
    the timer, so in your critical sections you **CAN NOT** use the timer or functions
    which uses the timer):
    ```
    nointerrupts();
    c = a + b; (16 bit non-atomic operation)
    interrupts();
    ```
    - side-effect: you can not use fading, or other PWM applications after a `nointerrupts();`
    because PWM is based on timer. So **better not to use functions based on timer** in a interrupt handler
    (`delay()` will not work in a function associated to an interrupt handler
- what is a pull-up/pull-down resistor and why it is used?
  - is a resistor used in a circuit to redirect a current ...?
  - bouncing ...?
- differenza tra macchine a stati finiti sincrone e asincrone
- scadere di un timer equivale ad una guardia?
  - no: le guardie sono eventi / condizioni
  - se la FSM è sincrona la valutazione dei tempi è implicita nella
    temporizzazione del polling
  - se la FSM è asincrona la temporizzazione si assume continua nel check della
    condizione di una guardia
- how do we chose the period for a FSM?
  - we can not afford to loose events (eg. a button press)
  - so we use MEST to provide samples in a delta-time lesser than the signal activation (GND-Vcc-GND) time for the button
  - period is chosen by the programmer; MEST is calculated empirically
    - if the period time is too small, we can have
      - performance issues by high consuption
      - synchronization issues with the FSM: the period time must NOT be lesser than the time used by a FSM step to complete
      - bouncing, glitches, spikes etc. recognized as a valid signal
        - in that case we need to register two more measurements to validate a measure
- why do we use concurrent states to represent a concurrent task in a FSM?
  - because if we don't use them, we should represent the same diagram by 
    drawing all the possible combinations for the state machine
- why do we use composed states in a state diagram?
  - to decouple tasks and responsibilities

#### Domande colleghi

- differenze tra polling e interrupt
- pwm
- come si calcolano le resistenze
- come funzionano gli interrupt handler
- macchine a stati finiti sincrone e asincrone
- come viene scelto il periodo
- bus di comunicazione seriale sincrono e asincrono
- cos’è l’esp e le sue caratteristiche
- cos’è un rtos e quali sono le sue principali caratteristiche
- async task quando vengono usati in android
- tipologie di mezzi trasmissivi e breve descrizione di essi
- come scegliere la resistenza per un led
- cos’è l'overrun
- resistenza di pull-up e di pull-down

## Project Rules

- https://iol.unibo.it/mod/page/view.php?id=260109
- the hardware kit can be brought at home if it is assigned to only one team
- assignments are NOT partial exams, they will be discussed and reviewed by the
  professor during the interview, NOT before
- the exam mark is NOT an average between the single assignments valuation
- you are NOT allowed to forget your project specification and implementation,
  even if you developed it months before
- all the projects can be done in group
- interview is personal
- each member is required to know everything about the project (not only its o
  n piece of work)

## Projects repos

- [1: Led 2 Bag](https://gitlab.com/p2f/seiot/project/01-led2bag)
  - [Assignment specification](https://docs.google.com/document/d/1tO3tTjGYKuKRHGeojmQGTlgJ-jMrJYdviDzXAEFtRFM/)
- [2: Smart Radar](https://gitlab.com/p2f/seiot/project/smart-radar)
  - [Assignment specification](https://docs.google.com/document/d/1N6KKlbnTpu6iX80Fo1OyMXKiCaPBwEGZT82N4j-cnik/edit?usp=sharing)
- [3: Smart Dumpster](https://gitlab.com/p2f/seiot/project/smart-dumpster)
  - [Assignment specification](https://docs.google.com/document/d/1nDHPowjjx0z_oTJF-qzlcCwXW8afP5O3o_KHDEHAcJQ/edit)

## Old projects

- [Progetto #1 - Led Pong](https://docs.google.com/document/d/e/2PACX-1vTwOl6e1oXjfxrX63BW5z4lmR93m-cKIpDLhDNd7RtmLD92rHAs-T6j81bpaGn2fDlWDHVdEkHhF-Oy/pub)
- [Progetto #2 - Smart Coffee Machine](https://docs.google.com/document/d/e/2PACX-1vRGdGbdKOTZGC-NrFwG06nxsjcTWIN54XVO17vYAzKb5PlYJLq1DWNT2pjRMIexDiLjqZpaP013mM_v/pub)
  - [Progetto #2 - Smart Coffee Machine - Note sulla Soluzione proposta](https://docs.google.com/document/d/1pxZTGjXRc6GDzE0O4gI74NnncRLx4Xe1nyN1aj0v-2k/edit)
- [Progetto #3 - Smart Greenhouse](https://docs.google.com/document/d/1c2YM09hkweuWbKUM2K8eYyVgazeHzXpe5MVtR51-K4g/edit)

## Group workflow

- balanced project work
- rotating management tasks
- meeting dates: monday and friday

## Project workflow

### Analysis

- first analysis (dynamic modeling: UML diagrams)
- software architecture analysis
- software implementation analysis (consider hardware limits & specifications)

### Circuit

Repeat until you get a working circuit:

- fritzing schema
- simulator implementation
- hardware implementation
- hardware test using [our test suite](https://gitlab.com/p2f/seiot/course/tree/master/hardware-test)

### Implementation

- [OO Projects] define interfaces
- [OO Projects] unit tests
- [OO Projects] write pipeline for CI/CD
- modular, top-down, documented software develop
- check documentation formatting (tabs, spacing, styling...)
- write report
- add authors name in all files
  ```
  /*
   * IoT 2019/2020
   * Assignment #1: Led to Bag
   * Authors:
   *  - Margotta Fabrizio
   *  - Mazzini Pietro
   *  - Righetti Franco
  */
  ```
- zip and deploy (use the [script](./deploy.sh))

## Tools

### Access USB

**NOTE**: do **NOT** run dev tools (Arduino IDE, PlatformIO IDE) as root.
This is strongly discouraged. If the above solution does not work,
find a fix and make a PR.

#### ArchLinux

- `gpasswd -a <USER> uucp`
- `gpasswd -a <USER> lock`
- logout, then login
- check executing `groups`

#### Ubuntu

[source](https://forum.arduino.cc/index.php?topic=495039.0)

- `sudo usermod -a -G dialout $USER`
- `sudo usermod -a -G uucp $USER`
- logout, then login
- check executing `groups`

### PlatformIO

PlatformIO on top of VSCode is the best option.

- install and open VSCode / VSCodium
- Go to extensions, then search and install
  - `PlatformIO`
  - `vscodevim`
- [install udev rules](https://docs.platformio.org/en/latest/faq.html#platformio-udev-rules)
- Activate PIO Core beta to get PIO Check module (only if the PIO Core version stable is < 4.1)
  - [reference](https://docs.platformio.org/en/latest/ide/vscode.html#platformio-ide-usedevelopmentpiocore)
  - Open VSCode, Settings (`Ctrl + ,`), search for `platformio-ide.useDevelopmentPIOCore` and set it to `true`
  - Restart VSCode
  - Wait for the new PIO Core installation
  - When asked, restart the View
  - Apply PIO Check module's settings to `platformio.ini` ([reference](https://docs.platformio.org/en/latest/plus/pio-check.html))
  - Use `pio check` in terminal to run tasks (static analysis and style check)

#### Code formatting

Use "Format document" task from VSCode.

If having problems like "Formatting failed", try with `sudo ln -s /usr/lib/libtinfo.so.6 /usr/lib/libtinfo.so.5` ([source](https://github.com/commercialhaskell/stack/issues/1012#issuecomment-369070365))

### Simulator

[Tinkercad Circuits](https://www.tinkercad.com/circuits)

### Fritzing (circuit sketch)

#### ArchLinux

- install `fritzing`
- [PIR sensor](https://sites.google.com/site/marcobotprojects/fritzing/pirsensor)

### Arduino

#### Arduino IDE (fallback)

[ArchLinux](https://wiki.archlinux.org/index.php/Arduino) instructions:
- install JDK 12: `jdk12-openjdk` ([why?](https://gitlab.com/p2f/seiot/course/issues/6#note_228251605))
- make JDK 12 as default: `sudo archlinux-java set java-12-openjdk` ([why?](https://github.com/arduino/Arduino/issues/8903))
- install `arduino` and `arduino-avr-core`
- open Arduino IDE and let it download its own stuff (updates & info...)
