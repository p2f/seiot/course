#!/bin/bash

FOLDER_NAME=Progetto-03

mkdir -p ${FOLDER_NAME}/{src,doc}

# src
git clone git@gitlab.com:p2f/seiot/project/smart-dumpster/controller.git ${FOLDER_NAME}/src/sd-controller
git clone git@gitlab.com:p2f/seiot/project/smart-dumpster/service.git ${FOLDER_NAME}/src/sd-service
git clone git@gitlab.com:p2f/seiot/project/smart-dumpster/app.git ${FOLDER_NAME}/src/sd-mobile-app
git clone git@gitlab.com:p2f/seiot/project/smart-dumpster/edge.git ${FOLDER_NAME}/src/sd-edge

# cleanup
rm -rf ${FOLDER_NAME}/src/sd-controller/.git
rm -rf ${FOLDER_NAME}/src/sd-service/.git
rm -rf ${FOLDER_NAME}/src/sd-mobile-app/.git
rm -rf ${FOLDER_NAME}/src/sd-edge/.git

# doc
wget https://gitlab.com/p2f/seiot/project/smart-dumpster/report/-/raw/master/main.pdf\?inline\=false -O Progetto-03/doc/Progetto-03.pdf

zip -r ${FOLDER_NAME}.zip ${FOLDER_NAME}/
rm -rf ${FOLDER_NAME}
