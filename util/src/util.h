/*
 * IoT 2019/2020
 * Assignment #1: Led to Bag
 * Authors:
 *  - Margotta Fabrizio
 *  - Mazzini Pietro
 *  - Righetti Franco
*/

#ifndef UNO_UTIL
#define UNO_UTIL

/* Include Arduino.h to solve
 *  unknown type name 'uint8_t'
 * https://community.platformio.org/t/errors-for-uint8-t-int32-t-and-int16-t-in-included-library-files/5899
 */
#include <Arduino.h>

/**
 * Initialize seed for random using circuit noise.
 * **NB**: only use Analog PINs.
 */
int initializeSeed(uint8_t analogPin);

/**
 * Blink a LED for specified duration.
 */
void blink(uint8_t led, unsigned long duration);

/**
 * Pulse a LED for specified duration with specified step.
 */
void pulse(uint8_t led, unsigned long duration, int step);

#endif /* UNO_UTIL */