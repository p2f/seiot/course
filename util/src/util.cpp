/*
 * IoT 2019/2020
 * Assignment #1: Led to Bag
 * Authors:
 *  - Margotta Fabrizio
 *  - Mazzini Pietro
 *  - Righetti Franco
*/

#include "L2BUtils.h"

int initializeSeed(uint8_t analogPin)
{
  if (analogPin >= 14 && analogPin <= 19)
  {
    randomSeed(analogRead(analogPin));
    return 1;
  }
  else
  {
    return 0;
  }
}

void blink(uint8_t led, unsigned long duration)
{
  digitalWrite(led, HIGH);
  delay(duration);
  digitalWrite(led, LOW);
}

void pulse(uint8_t led, unsigned long duration, int step)
{
  int currentIntensity = 0;
  unsigned long startingTime = millis();
  while (millis() - startingTime < duration)
  {
    analogWrite(led, currentIntensity);
    currentIntensity += step;
    if (currentIntensity <= 0 || currentIntensity >= 255)
    {
      step = -step;
    }
    delay(15);
  }
  step = step > 0 ? step : -step;
  while (currentIntensity > 0)
  {
    currentIntensity -= step;
    analogWrite(led, currentIntensity);
    delay(15);
  }
}